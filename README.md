A simple systemd service to play around with env vars to see what works.

## How to use
```bash
cd <this dir>
bash 01-setup.sh
bash 02-start-and-check.sh
# note the logs
bash 03-cleanup.sh
```

## Example output
```
Jan 25 14:00:37 pc systemd[911]: blah.service: Ignoring invalid environment assignment 'export BLAH_FIVE=exported': /home/user/git/systemd-demo/blah.env
Jan 25 14:00:37 pc systemd[911]: Started blah.service.
Jan 25 14:00:37 pc blah.sh[446344]: start
Jan 25 14:00:37 pc blah.sh[446344]: simple literal=11
Jan 25 14:00:37 pc blah.sh[446344]: multiline literal=first line
Jan 25 14:00:37 pc blah.sh[446344]:   second line indented
Jan 25 14:00:37 pc blah.sh[446344]: third line
Jan 25 14:00:37 pc blah.sh[446344]: with escape sequence=using " escape
Jan 25 14:00:37 pc blah.sh[446344]: no interpolation, quote preserved=${BLAH_ONE} doesn't work
Jan 25 14:00:37 pc blah.sh[446344]: can't find exported=
Jan 25 14:00:37 pc blah.sh[446344]: inner whitespace=inner white space preserved
Jan 25 14:00:37 pc blah.sh[446344]: --- all envs ---
Jan 25 14:00:37 pc blah.sh[446348]: BLAH_FOUR=${BLAH_ONE} doesn't work
Jan 25 14:00:37 pc blah.sh[446348]: BLAH_ONE=11
Jan 25 14:00:37 pc blah.sh[446348]: BLAH_SIX=inner white space preserved
Jan 25 14:00:37 pc blah.sh[446348]: BLAH_THREE=using " escape
Jan 25 14:00:37 pc blah.sh[446348]: BLAH_TWO=first line
Jan 25 14:00:37 pc blah.sh[446344]: end
```
