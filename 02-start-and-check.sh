#!/bin/bash
set -euxo pipefail

systemctl --user start blah
sleep 1
# note: due to https://bugzilla.redhat.com/show_bug.cgi?id=1426152 we might not see all
# the logs with
#   systemctl --user status blah
# ...so we just tail all logs
journalctl --since "5s ago" | grep blah
