#!/bin/bash
echo "start"
echo "simple literal=$BLAH_ONE"
echo "multiline literal=$BLAH_TWO"
echo "with escape sequence=$BLAH_THREE"
echo "no interpolation, quote preserved=$BLAH_FOUR"
echo "can't find exported=$BLAH_FIVE"
echo "inner whitespace=$BLAH_SIX"
echo "--- all envs ---"
env | grep BLAH | sort
echo "end"
