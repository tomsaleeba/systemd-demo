#!/bin/bash
set -euxo pipefail
cd "$(dirname "$0")"
thisDir=$PWD

mkdir -p ~/.config/systemd/user
cat << HEREDOC > ~/.config/systemd/user/blah.service
[Service]
ExecStart=$thisDir/blah.sh
EnvironmentFile=$thisDir/blah.env
HEREDOC

systemctl --user status blah
